# S1
en
conf t
h S1

int fa0/1
sw mod acc
7sw acc vlan 10

int fa0/10
sw mod acc
sw acc vlan 20

int fa0/20
sw mod acc
sw acc vlan 30

int fa0/24
sw mod tr
sw tr all vlan 10,20,30,99

# S2
en
conf t
h S2

int fa0/1
sw mod acc
sw acc vlan 10
int fa0/10
sw mod acc
sw acc vlan 20
int fa0/20
sw mod acc
sw acc vlan 30

int fa0/24
sw mod tr
sw tr all vlan 10,20,30,99


# S3
en
conf t
h S3

vlan 10
vlan 20
vlan 30
vlan 99

exit 

int range fa0/23-24,g0/1
sw mod tr
sw tr all vlan 10,20,30,99

end

# S4
en
conf t
h S4

vlan 12
vlan 22
vlan 32
vlan 99

exit 

int range fa0/23-24,g0/1
sw mod tr
sw tr all vlan 12,22,32,99

end

# S7
en
conf t
h S7

vlan 14
vlan 24
vlan 34
vlan 99

exit

int range fa0/23-24,g0/1
sw mod tr
sw tr all vlan 14,24,34,99

end

# S5
en
conf t
h S5

int fa0/1
sw mod acc
sw acc vlan 12

int fa0/10
sw mod acc
sw acc vlan 22

int fa0/20
sw mod acc
sw acc vlan 32

int fa0/24
sw mod tr
sw tr all vlan 12,22,32,99
end

# S6
en
conf t
h S6

int fa0/1
sw mod acc
sw acc vlan 12

int fa0/10
sw mod acc
sw acc vlan 22

int fa0/20
sw mod acc
sw acc vlan 32

int fa0/24
sw mod tr
sw tr all vlan 12,22,32,99
end

# S8
en
conf t
h S8

int fa0/1
sw mod acc
sw acc vlan 14

int fa0/10
sw mod acc
sw acc vlan 24

int fa0/20
sw mod acc
sw acc vlan 34

int fa0/24
sw mod tr
sw tr all vlan 14,24,34,99
end

# S9
en
conf t
h S9

int fa0/1
sw mod acc
sw acc vlan 14

int fa0/10
sw mod acc
sw acc vlan 24

int fa0/20
sw mod acc
sw acc vlan 34

int fa0/24
sw mod tr
sw tr all vlan 14,24,34,99
end


# ISP
en
conf t
h ISP

int g0/0
ip address 209.165.201.2 255.255.255.0
no shut 

int g0/1
ip address 10.0.10.1 255.255.255.0
no shut 

ip route 0.0.0.0 0.0.0.0 209.165.201.1


# R1
en
conf t
h R1

int g0/1
no shut 

int g0/1.10
encapsulation dot1q 10
ip add 192.168.10.1 255.255.255.0

int g0/1.20
encapsulation dot1q 20
ip add 192.168.20.1 255.255.255.0

int g0/1.30
encapsulation dot1q 30
ip add 192.168.30.1 255.255.255.0

int s0/0/0
ip add 172.16.10.1 255.255.255.252
no shut

int s0/0/1
ip add 172.16.10.9 255.255.255.252
no shut

ip dhcp pool GRN10
network 192.168.10.0 255.255.255.0
default-router 192.168.10.1

ip dhcp pool GRN20
network 192.168.20.0 255.255.255.0
default-router 192.168.20.1

ip dhcp pool GRN30
network 192.168.30.0 255.255.255.0
default-router 192.168.30.1

exit

ip dhcp excluded-address 192.168.10.1 192.168.10.10
ip dhcp excluded-address 192.168.20.1 192.168.20.10
ip dhcp excluded-address 192.168.30.1 192.168.30.10

ip route 0.0.0.0 0.0.0.0 209.165.201.2

int g0/0 
ip address 209.165.201.1 255.255.255.0
no shut
ip nat outside

int g0/1.10 
ip nat inside

int g0/1.20 
ip nat inside

int g0/1.30 
ip nat inside

access-list 10 permit 192.168.10.0 0.0.0.255 
ip nat inside source list 10 int g0/1.10 overload

access-list 20 permit 192.168.20.0 0.0.0.255 
ip nat inside source list 10 int g0/1.20 overload

access-list 30 permit 192.168.30.0 0.0.0.255 
ip nat inside source list 10 int g0/1.30 overload

router ospf 10
router-id 8.8.8.8
network 192.168.10.0 0.0.0.255 area 0
network 192.168.20.0 0.0.0.255 area 0
network 192.168.30.0 0.0.0.255 area 0
network 172.16.10.0 0.0.0.3 area 0
network 172.16.10.8 0.0.0.3 area 0
default-information originate

end

# R2 
en
conf t
h R2

int s0/0/0
ip add 172.16.10.2 255.255.255.252
no shut

int s0/0/1
ip add 172.16.10.5 255.255.255.252
no shut

int g0/1
no shut

int g0/1.12
encapsulation dot1q 12
ip add 192.168.12.1 255.255.255.0

int g0/1.22
encapsulation dot1q 22
ip add 192.168.22.1 255.255.255.0

int g0/1.32
encapsulation dot1q 32
ip add 192.168.32.1 255.255.255.0


ip dhcp pool AMS12
network 192.168.12.0 255.255.255.0
default-router 192.168.12.1

ip dhcp pool AMS22
network 192.168.22.0 255.255.255.0
default-router 192.168.22.1

ip dhcp pool AMS32
network 192.168.32.0 255.255.255.0
default-router 192.168.32.1

exit

ip dhcp excluded-address 192.168.12.1 192.168.12.10
ip dhcp excluded-address 192.168.22.1 192.168.22.10
ip dhcp excluded-address 192.168.32.1 192.168.32.10


access-list 110 deny icmp 192.168.10.0 0.0.0.255 192.168.0.0 0.0.255.255
access-list 110 permit ip any any
access-list 120 deny icmp 192.168.20.0 0.0.0.255 192.168.0.0 0.0.255.255
access-list 120 permit ip any any
access-list 130 deny icmp 192.168.30.0 0.0.0.255 192.168.0.0 0.0.255.255
access-list 130 permit ip any any

int g0/1.12
ip access-group 110 out 
int g0/1.22
ip access-group 120 out
int g0/1.32
ip access-group 130 out

router ospf 10
router-id 6.6.6.6
network 192.168.12.0 0.0.0.255 area 0
network 192.168.22.0 0.0.0.255 area 0
network 192.168.32.0 0.0.0.255 area 0
network 172.16.10.0 0.0.0.3 area 0
network 172.16.10.4 0.0.0.3 area 0

end


# R3
en
conf t
h R3

int s0/0/0
ip add 172.16.10.10 255.255.255.252
no shut

int s0/0/1
ip add 172.16.10.6 255.255.255.252
no shut

int g0/1
no shut

int g0/1.12
encapsulation dot1q 12
ip add 192.168.14.1 255.255.255.0

int g0/1.22
encapsulation dot1q 22
ip add 192.168.24.1 255.255.255.0

int g0/1.32
encapsulation dot1q 32
ip add 192.168.34.1 255.255.255.0


ip dhcp pool ZWO14
network 192.168.14.0 255.255.255.0
default-router 192.168.14.1

ip dhcp pool ZWO24
network 192.168.24.0 255.255.255.0
default-router 192.168.24.1

ip dhcp pool ZWO34
network 192.168.34.0 255.255.255.0
default-router 192.168.34.1

exit

ip dhcp excluded-address 192.168.14.1 192.168.14.10
ip dhcp excluded-address 192.168.24.1 192.168.24.10
ip dhcp excluded-address 192.168.34.1 192.168.34.10

router ospf 10
router-id 4.4.4.4
network 192.168.14.0 0.0.0.255 area 0
network 192.168.24.0 0.0.0.255 area 0
network 192.168.34.0 0.0.0.255 area 0
network 172.16.10.4 0.0.0.3 area 0
network 172.16.10.8 0.0.0.3 area 0
end
